// #name,parentName,isDirectory,content

var big =
[
    ["/", null, true, null],
    ["Notes", "/", true, null],
    ["Important", "/", true, null],
    [".Private", "/", true, null],
    ["readme.txt", "/", false, "MMC company server. Do NOT copy or export any files here."],
    ["schedule.txt", "Important", false, "From: aaron@mmc.com<br/>From now on you are supposed to arrive one hour earlier and leave one hour later.<br/>"],
    ["announcement.txt" ,"Important", false, "From: ellie@mmc.com<br/>From now on your mail passwords will have to be encrypted to ensure security in the company.<br/>Please make sure to change them soon.<br/>"],
    ["article.txt", ".Private", false, "The Local Post<br/>On September 23rd, Artie Mitchell (23) tragically died in a car crash near Hamilton.<br/>His friends and family will remember him."],
    ["newspaper.txt", "Notes", false, "The Local Post<br/>MMC SCIENCE :<br/>Founded by Aaron Mack, Artie Mitchell and Ellie Cooper in 2003, these three childhood friends are devoting their life to working on science and new technologies to change the world<br/>"]
]
