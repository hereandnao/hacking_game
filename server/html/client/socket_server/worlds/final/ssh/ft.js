//#name,parentName,isDirectory,content

var ft =
[
    ["/", null, true, null],
    ["Tests", "/", true, null],
    [".Students", "/", true, null],
    ["results.txt", "Tests", false, "#1 Artie Mitchell - Score 125 - Time 15mn 10s</br>#2 Nao Yoshida - Score 99 - Time 42mn 39s</br>#3 Pedro Ramirez - Score 97 - Time 45mn 22s"],
    [".yoshida.txt", ".Students", false, "Name: Nao Yoshida</br> IP : 9.80.45.122"],
    [".mitchell.txt", ".Students", false, "Name: Artie Mitchell</br> IP : 5.5.5.5"],
    [".ramirez.txt", ".Students", false, "Name: Pedro Ramirez</br> IP : 23.123.54.92"]
]
